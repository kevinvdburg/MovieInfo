/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

function onLoad() {
    document.addEventListener("deviceready", onDeviceReady, false);
}

// device APIs are available
//

function onDeviceReady() {
    // Now safe to use device APIs
}

$(document).ready(function() {
    $("#nameMovie").focus(function() {
        $("#SearchResults").css('display', 'block');
        $("#MovieResult").css('display', 'none');
    });
    $("#searchMovie").on('submit', function() {
        event.preventDefault();
        Search();
    });

    $("#nameMovie").on('change', function() {
        Search();
    });

    function Search() {
        var movieName = $("#nameMovie").val();
        console.log(movieName);
        $.ajax({
            url: 'http://www.omdbapi.com/?s=' + movieName,
            type: 'GET',
            dataType: 'json',
        })
            .done(function(json) {
                var movieList = "";
                $("#resultListItems").empty()
                if (json.Response == "False") {
                    $("<span class='error' style='position:relative; margin:0 auto'>Something went wrong - Try Again</span>").appendTo("#searchMovie").fadeOut(2500);
                    //$(".error").remove();
                } else {
                    $.each(json, function(index, val) {
                        //console.log(val);
                        $.each(val, function(index, val) {
                            //movieList += "<a href='#' data-imdb='" + val.imdbID + "' class='list-group-item movieItem'>" + val.Title + "<span class='badge'>" + val.Year + "</span></a>";
                            movieList += "<a href='#' data-imdb='" + val.imdbID + "' class='list-group-item movieItem'><table id='MovieListTable'><tr><td class='tdTitle'>" + val.Title + "</td><td class='tdBadge'><span class='badge'>" + val.Year + "</span></td></tr></table></a>";
                        });
                    });
                    $(".tdBadge")
                    $("#resultListItems").append(movieList);
                    $(".movieItem").on("click", function() {
                        ToggleSearch();
                        $(".MovieDetails").empty();
                        $(".MoviePosterDetails").remove();
                        $("#RotDefault").css('display', 'inline');
                        $("#RotTomato").css('display', 'none');
                        $("#RotRotten").css('display', 'none');
                        var imdb = $(this).data('imdb');
                        $.ajax({
                            url: 'http://www.omdbapi.com/?i=' + imdb + '&tomatoes=true',
                            type: 'GET',
                            dataType: 'json'
                        })
                            .done(function(json) {
                                if (json.Poster == "N/A") {
                                    $("#MoviePoster").parent().css('display', 'none');
                                } else {
                                    var MoviePoster = "<img class='MoviePosterDetails' src='" + json.Poster + "' alt='" + MovieTitle + "-Poster''>";
                                    $("#MoviePoster").parent().css('display', 'block');
                                    $("#MoviePoster").append(MoviePoster);
                                };
                                var MovieTitle = json.Title;
                                if (MovieTitle == "N/A") {
                                    $("#MovieTitle").parent().css('display', 'none');
                                } else {
                                    $("#MovieTitle").parent().css('display', 'inhert');
                                    $("#MovieTitle").append(MovieTitle);
                                };

                                var MovieYear = json.Year;
                                if (MovieYear == "N/A") {
                                    $("#MovieYear").parent().css('display', 'none');
                                } else {
                                    $("#MovieYear").parent().css('display', 'inhert');
                                    $("#MovieYear").append(MovieYear);
                                };
                                var MovieRated = json.Rated;
                                if (MovieRated == "N/A") {
                                    $("#MovieRated").parent().css('display', 'none');
                                } else {
                                    $("#MovieRated").parent().css('display', 'inhert');
                                    $("#MovieRated").append(MovieRated);
                                };
                                var MovieReleased = json.Released;
                                if (MovieReleased == "N/A") {
                                    $("#MovieReleased").parent().css('display', 'none');
                                } else {
                                    $("#MovieReleased").parent().css('display', 'inhert');
                                    $("#MovieReleased").append(MovieReleased);
                                };
                                var MovieRuntime = json.Runtime;
                                if (MovieRuntime == "N/A") {
                                    $("#MovieRuntime").parent().css('display', 'none');
                                } else {
                                    $("#MovieRuntime").parent().css('display', 'inhert');
                                    $("#MovieRuntime").append(MovieRuntime);
                                };
                                var MovieGenre = json.Genre;
                                if (MovieGenre == "N/A") {
                                    $("#MovieGenre").parent().css('display', 'none');
                                } else {
                                    $("#MovieGenre").parent().css('display', 'inhert');
                                    $("#MovieGenre").append(MovieGenre);
                                };
                                var MovieDirector = json.Director;
                                if (MovieDirector == "N/A") {
                                    $("#MovieDirector").parent().css('display', 'none');
                                } else {
                                    $("#MovieDirector").parent().css('display', 'inhert');
                                    $("#MovieDirector").append(MovieDirector);
                                };
                                var MovieWriter = json.Writer;
                                if (MovieWriter == "N/A") {
                                    $("#MovieWriter").parent().css('display', 'none');
                                } else {
                                    $("#MovieWriter").parent().css('display', 'inhert');
                                    $("#MovieWriter").append(MovieWriter);
                                };
                                var MovieActors = json.Actors;
                                if (MovieActors == "N/A") {
                                    $("#MovieActors").parent().css('display', 'none');
                                } else {
                                    $("#MovieActors").parent().css('display', 'inhert');
                                    $("#MovieActors").append(MovieActors);
                                };
                                var MoviePlot = json.Plot;
                                if (MoviePlot == "N/A") {
                                    $("#MoviePlot").parent().css('display', 'none');
                                } else {
                                    $("#MoviePlot").parent().css('display', 'inhert');
                                    $("#MoviePlot").append(MoviePlot);
                                };
                                var MovieLanguage = json.Language;
                                if (MovieLanguage == "N/A") {
                                    $("#MovieLanguage").parent().css('display', 'none');
                                } else {
                                    $("#MovieLanguage").parent().css('display', 'inhert');
                                    $("#MovieLanguage").append(MovieLanguage);
                                };
                                var MovieCountry = json.Country;
                                if (MovieCountry == "N/A") {
                                    $("#MovieCountry").parent().css('display', 'none');
                                } else {
                                    $("#MovieCountry").parent().css('display', 'inhert');
                                    $("#MovieCountry").append(MovieCountry);
                                };
                                var MovieAwards = json.Awards;
                                if (MovieAwards == "N/A") {
                                    $("#MovieAwards").parent().css('display', 'none');
                                } else {
                                    $("#MovieAwards").parent().css('display', 'inhert');
                                    $("#MovieAwards").append(MovieAwards);
                                };

                                //--------------Score-------------------\\

                                //IMDB
                                var MovieT = json.Title;
                                var IMDBScore = json.imdbRating;
                                var IMDBID = json.imdbID;
                                console.log(MovieT);
                                console.log(IMDBScore);
                                console.log(IMDBID);

                                if (IMDBScore == "N/A") {
                                    $("#RatingIMDB").css('display', 'none');
                                } else {
                                    $("#RatingIMDB").css('display', 'block');
                                    $("#IMDBScore").text(json.imdbRating);
                                };
                                $("#Share").on('click',function(){
                                    window.plugins.socialsharing.share('I found this movie using MovieSearch go check it out!:, '+MovieT+', '+json.Poster+'', MovieT, json.Poster, 'http://goo.gl/8X665U')
                                });
                                //IMDB Click
                                $("#RatingIMDB").on('click', function(event) {
                                    var IMDBurl = "http://www.imdb.com/title/" + IMDBID + "/";
                                    $("#IMDBLink").attr('href', IMDBurl);
                                });


                                //Rotten
                                var RottonScore = json.tomatoRating;
                                var Rot = RottonScore.split(".").join("");
                                if (RottonScore == "N/A") {
                                    $("#RatingRottenTomatoes").css('display', 'none');
                                } else {
                                    var Rot = parseInt(RottonScore.split(".").join(""));
                                    $("#RotDefault").css('display', 'none');
                                    if (Rot <= 59) {
                                        $("#RotRotten").css('display', 'inline');
                                    } else {
                                        $("#RotTomato").css('display', 'inline');
                                    };

                                    $("#RatingRottenTomatoes").css('display', 'block');
                                    $("#RottonScore").text(RottonScore);
                                };
                                //Rotten Click
                                $("#RatingRottenTomatoes").on('click', function(event) {
                                    var TitleStripRotten = MovieT.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '+').toLowerCase();
                                    var Rottenurl = "http://www.rottentomatoes.com/search/?search=" + TitleStripRotten + "&sitesearch=rt";
                                    $("#RottenLink").attr('href', Rottenurl);
                                });

                                //Meta
                                var MetascoreScore = json.Metascore;
                                if (MetascoreScore == "N/A") {
                                    $("#RatingMetascore").css('display', 'none');
                                } else {
                                    $("#RatingMetascore").css('display', 'block');
                                    $("#MetascoreScore").text(MetascoreScore);
                                    var ParseMetascoreScore = parseInt(json.Metascore);

                                    if (ParseMetascoreScore <= 39) {
                                        $("#MetascoreScore").css('background-color', '#f00');
                                    } else if (ParseMetascoreScore >= 40 && ParseMetascoreScore <= 60) {
                                        $("#MetascoreScore").css('background-color', '#fc3');
                                    } else if (ParseMetascoreScore <= 100) {
                                        $("#MetascoreScore").css('background-color', '#6c3');
                                    } else {
                                        $("#MetascoreScore").css('background-color', '#6c3');
                                    };
                                };
                                //Meta Click
                                $("#RatingMetascore").on('click', function(event) {
                                    var TitleStripMeta = MovieT.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '+').toLowerCase();
                                    var Metascoreurl = "http://www.metacritic.com/search/movie/" + TitleStripMeta + "/results";
                                    $("#MetaLink").attr('href', Metascoreurl);
                                });
                            })
                            .fail(function() {
                                console.log("error");
                            });
                    });
                };

            })
            .fail(function() {
                console.log("error");
            });
    }

    function ToggleSearch() {
        if ($("#SearchResults:visible")) {
            $("#SearchResults").css('display', 'none');
            $("#MovieResult").css('display', 'block');
        } else {
            $("#SearchResults").css('display', 'block');
            $("#MovieResult").css('display', 'none');
        }

    }



});
